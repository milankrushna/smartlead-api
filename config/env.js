const ACTIVE_ENV = (process.env.NODE_ENV) ? process.env.NODE_ENV : "development";
console.log("envRuning on", ACTIVE_ENV);

const ENVIRONMENT_SETUP = require(`./../environment/env.${ACTIVE_ENV}.js`);

//server Port
exports.PORT = ENVIRONMENT_SETUP.PORT;

//PG cred
exports.pgSQLEnvironment = ENVIRONMENT_SETUP.pgSqlDBenvironment;
//AWS Email SDK cred
exports.sesCred = ENVIRONMENT_SETUP.sesCred;