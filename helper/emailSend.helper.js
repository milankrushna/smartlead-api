const nodemailer = require("nodemailer");

exports.sendEmailToAddress = (mailConfigData) => {
    console.log(mailConfigData);

    let mailConfig = {
        host: mailConfigData.smtp_host,
        port: mailConfigData.smtp_port,
        auth: {
            user: mailConfigData.user_name,
            pass: mailConfigData.password
        }
    };


    return new Promise((resolve, reject) => {
        try {


            let transporter = nodemailer.createTransport(mailConfig);
            // Message object
            let message = {
                from: mailConfigData.from_email,
                to: mailConfigData.send_to_email,
                subject: mailConfigData.subject,
                html: mailConfigData.email_body
            };

            transporter.sendMail(message, (err, info) => {
                if (err) {
                    console.log("sendEmail=>", err.message);
                    reject(err);
                    return;
                }

                console.log("Message sent:", info.messageId);
                console.log("Preview URL: ", nodemailer.getTestMessageUrl(info));
                resolve({ status: 1, message: "Email Sent successfully", data: info });
                return;
            });

        } catch (error) {

            reject({ status: 0, message: "error in Email Sending", date: error });
        }
    });

};