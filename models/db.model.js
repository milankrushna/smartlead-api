/**
 * PQSQL Query exec
 */
const { client } = require("../cred/pgsql");
const { ACCOUNT_DB, EMAIL_SEND } = require("../config/pgTable.config");
// var events = require("events");
// var eventEmitter = new events.EventEmitter();

/**
 * new Account date insert
 * @param {*} insertObj 
 */
exports.createAccountData = (insertObj) => {

    return new Promise((resolve, reject) => {
        // returning Promise for better control
        return client.query(`
        INSERT INTO ${ACCOUNT_DB} (
            from_name,
            from_email,
            user_name,
            password,
            smtp_host,
            smtp_port,
            smtp_com_type,
            message_per_day,
            min_time_gap,
            is_diff_replay,
            is_diff_email_to_rcv,
            imap_host,
            imap_port,
            imap_com_type,
            create_date,
            update_date) 
            VALUES (
                $1, $2, $3, $4,
                $5, $6, $7, $8,
                $9, $10, $11, $12,
                $13, $14, $15, $16
                )`, insertObj)
            .then((res) => {
                console.log(res.command, "Data Successfully Inserted", JSON.stringify(insertObj));
                resolve({ status: 1, message: "Data Successfully Inserted" });

            })
            .catch((err) => {
                console.log("Data insert Error", err);
                reject({ status: 0, message: "Error In Insert - " + err.message });
            });

    });

};


exports.storeEmail = (insertObj) => {

    console.log(insertObj);

    return new Promise((resolve, reject) => {
        // returning Promise for better control
        return client.query(`
        INSERT INTO ${EMAIL_SEND} (
            to_email,
            email_body,
            subject,
            cc_address,
            bcc_address,
            sent_time,
            from_id
            ) 
            VALUES (
                $1, $2, $3, $4,
                $5, $6, $7
                )`, insertObj)
            .then((res) => {
                console.log(res);
                console.log(res.command, "Data Successfully email Send Inserted", JSON.stringify(insertObj));

                resolve({ status: 1, message: "Data Successfully Inserted" });

            })
            .catch((err) => {
                console.log("Data insert Error", err);
                reject({ status: 0, message: "Error In Insert - " + err.message });
            });

    });

};


/**
 * fetching USER Login details
 * @param {*} loginId 
 * @returns 
 */
exports.fetchLoginDetails = (loginId) => {

    // returning Promise for better control
    return new Promise((resolve, reject) => {


        let selectQuery = `SELECT id,from_email,user_name,password,smtp_host,smtp_port,smtp_com_type,message_per_day FROM ${ACCOUNT_DB} WHERE id = $1`;

        return client.query(selectQuery, [loginId])
            .then((resp) => {
                console.log(resp.command, "Data Successfully Fetched");

                resolve({ status: 1, message: "Data Successfully Fetched", data: resp["rows"][0] });

            })
            .catch((err) => {
                console.log("Error in fetching account details", err);
                reject({ status: 0, message: "Error In Insert - " + err.message });
            });

    });

}