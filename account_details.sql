-- -------------------------------------------------------------
-- TablePlus 5.1.0(468)
--
-- https://tableplus.com/
--
-- Database: Test
-- Generation Time: 2023-03-01 02:54:05.3350
-- -------------------------------------------------------------


-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS account_details_id_seq;

-- Table Definition
CREATE TABLE "public"."account_details" (
    "id" int4 NOT NULL DEFAULT nextval('account_details_id_seq'::regclass),
    "from_name" varchar,
    "from_email" varchar,
    "user_name" varchar,
    "password" varchar,
    "smtp_host" varchar,
    "smtp_port" varchar,
    "smtp_com_type" varchar,
    "message_per_day" varchar,
    "min_time_gap" varchar,
    "is_diff_replay" bool,
    "is_diff_email_to_rcv" bool,
    "imap_host" varchar,
    "imap_port" varchar,
    "imap_com_type" varchar,
    "create_date" timestamptz,
    "update_date" timestamptz,
    PRIMARY KEY ("id")
);

INSERT INTO "public"."account_details" ("id", "from_name", "from_email", "user_name", "password", "smtp_host", "smtp_port", "smtp_com_type", "message_per_day", "min_time_gap", "is_diff_replay", "is_diff_email_to_rcv", "imap_host", "imap_port", "imap_com_type", "create_date", "update_date") VALUES
(1, 'Milan Krushna Behera', 'milankrushna@gmail.com', 'mialnkrushna', 'a6954f3348322096ecba0689ca71a827', 'smtp.zoho.com', '465', 'SSL', '200', '23', 't', 't', 'imap.zoho.com', '993', 'SSL', '2023-02-02 10:39:40+00', '2023-02-02 10:39:40+00'),
(3, 'Milan Krushna', 'milan@mail.com', 'milankrusj', '1234567', 'smtp.zoho.com', '465', 'TLS', '200', '12', 'f', 'f', 'imap.zoho.com', '993', 'TLS', '2023-03-03 02:16:21+00', '2023-03-03 02:16:21+00'),
(4, 'Milan Krushna', 'milan@mail.com', 'milankrusjer', '1234567', 'smtp.zoho.com', '465', 'TLS', '200', '12', 'f', 'f', 'imap.zoho.com', '993', 'TLS', '2023-03-03 02:20:18+00', '2023-03-03 02:20:18+00'),
(5, 'Milan Krushna', 'milan@mail.com', 'milankru1234', '1234567', 'smtp.zoho.com', '465', 'TLS', '200', '12', 'f', 'f', 'imap.zoho.com', '993', 'TLS', '2023-03-03 02:20:40+00', '2023-03-03 02:20:40+00');
