const { validationResult } = require("express-validator");

exports.processValidation = (req, res, next) => {
    console.log("RequestIp", req.connection.remoteAddress);
    try {
        const errors = validationResult(req);
        // console.log(errors);
        if (!errors.isEmpty()) {
            console.log(JSON.stringify(errors.array()));
            return res.status(422).json({ status: 0, message: `Invalid Entity - ${Object.keys(errors.mapped()).join()}`, data: "", errors: errors.array() });
        }

    } catch (error) {
        return res.status(422).json({ status: 0, message: error.message, data: "" });
    }
    next();
    return true;
};