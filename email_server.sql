-- -------------------------------------------------------------
-- TablePlus 5.1.0(468)
--
-- https://tableplus.com/
--
-- Database: Test
-- Generation Time: 2023-03-01 02:53:48.2520
-- -------------------------------------------------------------


-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS email_server_id_seq;

-- Table Definition
CREATE TABLE "public"."email_server" (
    "id" int4 NOT NULL DEFAULT nextval('email_server_id_seq'::regclass),
    "from_id" varchar,
    "email_body" text,
    "sent_time" timestamptz,
    "subject" varchar,
    "is_sent" bool,
    "cc_address" json,
    "bcc_address" json,
    "to_email" json,
    "received_time" timestamptz,
    PRIMARY KEY ("id")
);

INSERT INTO "public"."email_server" ("id", "from_id", "email_body", "sent_time", "subject", "is_sent", "cc_address", "bcc_address", "to_email", "received_time") VALUES
(1, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:18:55+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(2, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:20:30+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(3, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:20:40+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(4, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:21:42+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(5, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:27:54+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(6, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:28:54+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(7, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:31:26+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(8, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:32:17+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(9, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:33:12+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(10, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:35:27+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(11, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:38:23+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(12, '1', '<p>Sending a test emial to my client</p>', '2023-03-03 12:40:18+00', 'Creating a test emial', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milank@gmail.com"]}', NULL),
(13, '1', 'Please find a testEmail', '2023-03-03 02:31:20+00', 'Test Email Subject', NULL, '{"cc_address":["vaibhav@five2one.com.au"]}', '{"bcc_address":[]}', '{"send_to_email":["milantest@gmail.com"]}', NULL);
