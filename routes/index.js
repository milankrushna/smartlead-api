var express = require("express");
var router = express.Router();
const emailController = require("../controllers/email.controller");
const { body } = require("express-validator");
const validation = require("../middleware/expValidator");

/**
 * create Account Route
 */
router.post("/create_account",

    // declearing some minimal req body Validation
    [
        body("from_name")
            .notEmpty()
            .withMessage("from_name Required!"),

        body("from_email")
            .notEmpty()
            .withMessage("from_email Required!")
            .isEmail()
            .withMessage("Please pass a valid email Id"),

        body("user_name")
            .notEmpty()
            .withMessage("user_name Required!"),

        body("password")
            .notEmpty()
            .withMessage("password Required!"),

        body("smtp_host")
            .notEmpty()
            .withMessage("smtp_host Required!"),

        body("smtp_port")
            .notEmpty()
            .withMessage("smtp_port Required!"),

        body("smtp_com_type")
            .notEmpty()
            .withMessage("smtp_com_type Required!"),

        body("message_per_day")
            .notEmpty()
            .withMessage("message_per_day Required!"),

        body("min_time_gap")
            .notEmpty()
            .withMessage("min_time_gap Required!"),

        body("is_diff_replay")
            .notEmpty()
            .withMessage("is_diff_replay Required!"),

        body("is_diff_email_to_rcv")
            .notEmpty()
            .withMessage("is_diff_email_to_rcv Required!"),

        body("imap_host")
            .notEmpty()
            .withMessage("imap_host Required!"),

        body("imap_port")
            .notEmpty()
            .withMessage("imap_port Required!"),

        body("imap_com_type")
            .notEmpty()
            .withMessage("imap_com_type Required!"),

    ],
    //processing the validation if any validation is getting failed
    validation.processValidation,
    emailController.createAccount);




/**
 * Send Email Route
 */
router.post("/send_email",

    // declearing some minimal req body Validation
    [
        body("subject")
            .notEmpty()
            .withMessage("Subject Required!")
            .isLength(5)
            .withMessage("Subject should be minimum 5 character"),

        body("to_email")
            .notEmpty()
            .withMessage("from_email Required!")
            .isEmail()
            .withMessage("Please pass a valid email Id"),

        body("email_body")
            .notEmpty()
            .withMessage("user_name Required!"),


    ],
    //processing the validation if any validation is getting failed
    validation.processValidation,
    emailController.sendEmail);

module.exports = router;
