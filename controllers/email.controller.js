const moment = require("moment");
const DBModel = require("./../models/db.model")
// const crypto = require("crypto");
const emailHelper = require("./../helper/emailSend.helper");




/**
 * Process Create Account Data
 * @param {*} req 
 * @param {*} res 
 */
exports.createAccount = (req, res) => {

    try {

        let {
            from_name,
            from_email,
            user_name,
            password,
            smtp_host,
            smtp_port,
            smtp_com_type,
            message_per_day,
            min_time_gap,
            is_diff_replay,
            is_diff_email_to_rcv,
            imap_host,
            imap_port,
            imap_com_type

        } = req.body;


        //preparing the date to insert into DB
        let create_date = moment().format("Y-MM-d h:m:s"),
            update_date = moment().format("Y-MM-d h:m:ss");
        // password = crypto.createHash("md5").update(password).digest("hex");

        let accountProp = [
            from_name.trim(),
            from_email.trim().toLowerCase(),
            user_name.trim(),
            password,
            smtp_host.trim(),
            smtp_port,
            smtp_com_type,
            message_per_day,
            min_time_gap,
            is_diff_replay,
            is_diff_email_to_rcv,
            imap_host,
            imap_port,
            imap_com_type,
            create_date,
            update_date

        ];

        DBModel.createAccountData(accountProp).then((resp) => {
            res.status(200).json(resp);
        }).catch(err => {
            res.status(400).json(err);
        });


    } catch (error) {
        res.status(422).json({ status: -1, message: error.message });
    }
};


exports.sendEmail = (req, res) => {

    try {

        let {
            subject,
            to_email,
            email_body

        } = req.body;

        //currently login id is hardcoded
        let login_id = 1;

        //will fetch the LoginDetails to send the Email
        DBModel.fetchLoginDetails(login_id).then((loginAccountDetails => {

            console.log({ loginAccountDetails });


            //preparing the date to insert into DB
            let create_date = moment().format("Y-MM-d h:m:s");

            //if multiple Email will be come for (toemail,cc and bcc) then it should be seperated by (,)
            let send_to_email = to_email.split(",");
            //for Now we are doing it static
            let cc_address = ["vaibhav@five2one.com.au"];
            let bcc_address = [];

            let emailSendingProp = [
                { send_to_email },
                email_body,
                subject,
                { cc_address },
                { bcc_address },
                create_date,
                login_id

            ];

            DBModel.storeEmail(emailSendingProp).then((sendEmailDbResp) => {
                console.log({ sendEmailDbResp });
                let { smtp_host, smtp_port, user_name, password, from_email } = loginAccountDetails.data;
                //preparing the Email sending object
                var emailObj = {
                    subject,
                    email_body,
                    send_to_email,
                    cc_address,
                    bcc_address,
                    from_email,
                    smtp_host,
                    smtp_port,
                    user_name,
                    password
                };

                emailHelper.sendEmailToAddress(emailObj).then((resp) => {
                    console.log("realmailLog", { status: 1, message: "Email sent successfully!", data: resp });

                    //if email Sent successfully update the DB

                    res.status(200).json({ status: 1, message: "Email sent successfully!", data: resp });

                }).catch(error => {
                    console.log("realmailError", { status: 0, message: error.message, data: error });
                    res.status(400).json({ status: 0, message: error.message, data: error });

                });



            }).catch(err => {
                res.status(400).json(err);
            });

        })).catch(error => {
            res.status(400).json({ status: -1, message: "Email sending failed, Please try after sometime" });
        });

    } catch (error) {
        res.status(422).json({ status: -1, message: "Email sending failed, Please try after sometime" });
    }
};

