/**
 * Developemnt Environment Configuration
 */
module.exports = {
    PORT: 8080,

    //pgsql Cred
    pgSqlDBenvironment: {
        pgsql_host: "",
        pgsql_port: 5432,
        pgsql_database: "",
        pgsql_user: "",
        pgsql_password: "",
        pgsql_allowExitOnIdle: true,

    }
};
